/* include libraryes */
#include "SPI.h" 
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
/* define things */
#define TFT_DC 9
#define TFT_CS 10
#define TFT_RST 8
#define CS_PIN  7
/* define colors */
#define black 0x0000
#define blue 0x001f
#define red 0xf800
#define green 0x7e0
#define cyan 0x07ff
#define yellow 0xffe0
/* ends */
#include <XPT2046_Touchscreen.h>
XPT2046_Touchscreen ts(CS_PIN);
#define TIRQ_PIN  2
/* needed */
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
#include "heb_font.h"
byte key,i;
int x,y;
boolean wastouched = true;
/* key touch */
void Keytouch()
{
boolean istouched = ts.touched();
  if (istouched)
  {
    TS_Point p = ts.getPoint(); 
    x=p.x; 
    y=p.y;
     if (!wastouched)
     {
      /* tft. */
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key Start */
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 0 */ 
        key = 0;
      }
      if (x > 2300 && x < 2700 && y > 800 && y < 1200)
      {
        /* key 1 */
        key = 1;
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 2 */
         key = 2;
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 3 */
        key = 3;
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 4 */
        key = 4;
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 5 */
        key = 5;
      }     
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 6 */
        key = 6;
      } 
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 7 */ 
        key = 7;
      }  
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 8 */
        key = 8;
      }
      if (x > [] && x < [] && y > [] && y < [])
      {
        /* key 9 */
        key = 9;
      }
      tft.fillRect(0,0,320,30,BLACK);
      tft.setCursor(5,10);
      tft.setTextColor(GREEN);
      tft.setTextSize(2);
      String val;
      val=("X ")+String(p.x)+(" Y ")+String(p.y)+" "+String(key);
      tft.println(val);
     }
  }
   wastouched = istouched;
  delay(100);
}
/* void of the app */
void setup() 
{
  pinMode(TFT_RST ,OUTPUT);
  digitalWrite(TFT_RST, 1);
  delay(10);
  digitalWrite(TFT_RST, 0);
  delay(10);
  digitalWrite(TFT_RST, 1);
  delay(10);
  tft.begin();
  ts.begin();
  
  /* fill screen */
  tft.fillScreen(black);
  tft.setRotation(4); /*1*/
  
  /* buttons */
  tft.setTextSize(4);  
  /* tft.drawRect(x0,y0,high,width,color) */
  
  /* btn accept */
  tft.drawRect(170,270,50,45,red);
  tft.setCursor(185,285);
  tft.setTextColor(green);
  tft.print("*");
  
  /* btn 0 */
  tft.drawRect(95,270,50,45,red);
  tft.setCursor(110,285);
  tft.setTextColor(green);
  tft.print("0");
  
  /* btn 1 */ 
  tft.drawRect(20,100,50,45,red);
  tft.setCursor(35,110);
  tft.setTextColor(green);
  tft.print("1"); 

  /* btn 2 */
  tft.drawRect(95,100,50,45,red);
  tft.setCursor(110,110);
  tft.setTextColor(green);
  tft.print("2");
  
  /* btn 3 */
  tft.drawRect(170,100,50,45,red);
  tft.setCursor(185,110);
  tft.setTextColor(green);
  tft.print("3");
  
  /* btn 4 */
  tft.drawRect(20,155,50,45,red);
  tft.setCursor(35,165);
  tft.setTextColor(green);
  tft.print("4");
  
  /* btn 5 */
  tft.drawRect(95,155,50,45,red);
  tft.setCursor(110,165);
  tft.setTextColor(green);
  tft.print("5");
  
  /* btn 6 */
  tft.drawRect(170,155,50,45,red);
  tft.setCursor(185,165);
  tft.setTextColor(green);
  tft.print("6");
  
  /* btn 7 */
  tft.drawRect(20,210,50,45,red);
  tft.setCursor(35,220);
  tft.setTextColor(green);
  tft.print("7");
  
  /* btn 8 */
  tft.drawRect(95,210,50,45,red);
  tft.setCursor(110,220);
  tft.setTextColor(green);
  tft.print("8");
  
  /* btn 9 */
  tft.drawRect(170,210,50,45,red);
  tft.setCursor(185,220);
  tft.setTextColor(green);
  tft.print("9");
}
/* loop to all */
void loop() 
{
  Keytouch();
  
}