
//**************************
//   ?????? ???? ?? ?????? ??????
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

//  ???? ??????? ??????   
char hebrewTavs[28][8]= {
{0x11,0x11,0x9,0x0d,0x12,0x11,0x11,0},{0x1e,2,2,2,2,2,0x1f,0},{0x1f,1,1,3,9,0x11,0x11,0},{0x1f,2,2,2,2,2,2,0},   //  ? ? ? ?
{0x1f,1,1,1,0x11,0x11,0x11,0}, {0x0e,2,2,2,2,2,2,0},{0x1f,4,4,8,8,8,8,0},{0x1f,0x11,0x11,0x11,0x11,0x11,0x11,0},   //   ? ? ? ? 
{0x11,0x13,0x15,0x11,0x11,0x11,0x1f,0},{0x0e,2,2,2,0,0,0,0},{0x1f,1,1,1,1,1,1,1},{0x1f,1,1,1,1,1,0x1f,0}, //  ? ? ?  ?  
{0x10,0x1f,1,1,2,4,8,0},{0x1f,0x11,0x11,0x11,0x11,0x11,0x1f,0},{0x10,0x13,0x0d,0x9,0x11,0x11,0x17,0},{0x0e,2,2,2,2,2,2,2},//  ? ? ? ?  
{3,1,1,1,1,1,0x1f,0},{0x1f,9,9,9,9,0x9,0x06},{9,9,9,9,5,5,0x1f,0},{0x1f,0x11,0x11,0x0d,1,1,1,0},{0x1f,9,9,0x0d,1,1,0x1f,0}, // ?   ?  ?  ?  ?  
{0x11,0x11,0x0a,4,2,1,0,0},{0x11,0x11,0x0a,4,2,1,0x1f,0},{0x1f,1,1,9,9,0xa,8,8},{0x1f,1,1,1,1,1,1,0},       //  ?  ?   ?  ? 
{0x15,0x15,0x15,0x15,0x15,0x15,0x1f,0},{0x0f,9,9,9,9,9,0x19,0},           //  ?   ?
{0,0,0,0,0,0,0,0}    //  ???? ??? ????         
          };

//   ??????? ????? ??. ??? ????? ?? ??????  ??????? ???? ????. ??? ????? ?? ????? ???????????? ???? ????? 
// ?????? ???? ? ?????. ??? ????? ?? ??? ??????  ?? ??? ??? ??? ???? ?????   

void drawHebrewTav(int x, int y,byte *tav, int color,int tavSize);
void drawHebrewString(int x,int y,byte st[],int Color,int tavSize);


void drawHebrewTav(int x, int y,byte *tav, int color,int tavSize)
{
  int x0=x,y0=y,i,j,sizeX,sizeY;
  byte check;
   for (i=0;i<8;i++,tav++)
   for(sizeY=0;sizeY<tavSize;sizeY++,y0++)
    for(x0=x,check=16,j=0; j<5;x0=x0+tavSize,check=check>>1,j++)
      if(*tav &check)
       for(sizeX=0;sizeX<tavSize;sizeX++)
         tft.drawPixel(x0+sizeX,y0,color);
}
void drawHebrewString1(int x,int y,byte *ptr,int color,int tavSize)
{
   char englishTav;
   for(int x0=x;*ptr;ptr++,x0=x0+tavSize*5+tavSize)
  {
   if(*ptr >= 0x80 )
      drawHebrewTav(x0,y,hebrewTavs[*ptr-0x90],color,tavSize);
   else if(*ptr==' ')   //   ??? ??? ??? ????
      drawHebrewTav(x0,y,hebrewTavs[27],color,tavSize);  //  ????? ????
    else
    {
    englishTav=*ptr;
    tft.setCursor(x0, y);
    tft.setTextColor(color); 
    tft.setTextSize(tavSize);
    tft.print(englishTav);
    x0=x0+tavSize*5+tavSize;
    }
   }
}

  
void drawHebrewString(int x,int y,byte *ptr,int color,int tavSize)
{
   char englishTav;
   byte *ptr1=ptr;
   while(*ptr++);
   ptr=ptr-2;
   for(int x0=x;ptr>ptr1;ptr--,x0=x0+tavSize*5+tavSize)
  {
    if(*ptr>0xb0)
      ptr--;
    if(*ptr > 0x80 )
      drawHebrewTav(x0,y,hebrewTavs[*ptr-0x90],color,tavSize);
    else if(*ptr==' ')   //   ??? ??? ??? ????
      drawHebrewTav(x0,y,hebrewTavs[27],color,tavSize);  //  ????? ????
    else
    {
    englishTav=*ptr;
    tft.setCursor(x0, y);
    tft.setTextColor(color); 
    tft.setTextSize(tavSize);
    tft.print(englishTav);
    x0=x0+tavSize*5+tavSize;
    }
   }
}

