#ifndef __BIT_DEF_H__
#define __BIT_DEF_H__

#include <avr/io.h>

typedef struct
{
  unsigned int bit0:1;
  unsigned int bit1:1;
  unsigned int bit2:1;
  unsigned int bit3:1;
  unsigned int bit4:1;
  unsigned int bit5:1;
  unsigned int bit6:1;
  unsigned int bit7:1;
} _io_reg;

#define REG_B(rg,bt) ((volatile _io_reg*)&rg)->bit##bt

#define sbi(port, bit) port |= _BV(bit)  
#define cbi(port, bit) port &= ~(_BV(bit))

/* PORT A */
#define PORTA_0	REG_B(PORTA,0)
#define PORTA_1	REG_B(PORTA,1)
#define PORTA_2	REG_B(PORTA,2)
#define PORTA_3	REG_B(PORTA,3)
#define PORTA_4	REG_B(PORTA,4)
#define PORTA_5	REG_B(PORTA,5)
#define PORTA_6	REG_B(PORTA,6)
#define PORTA_7	REG_B(PORTA,7)

#define PINA_0	REG_B(PINA,0)
#define PINA_1	REG_B(PINA,1)
#define PINA_2	REG_B(PINA,2)
#define PINA_3	REG_B(PINA,3)
#define PINA_4	REG_B(PINA,4)
#define PINA_5	REG_B(PINA,5)
#define PINA_6	REG_B(PINA,6)
#define PINA_7	REG_B(PINA,7)
           
#define DDRA_0	REG_B(DDRA,0)
#define DDRA_1	REG_B(DDRA,1)
#define DDRA_2	REG_B(DDRA,2)
#define DDRA_3	REG_B(DDRA,3)
#define DDRA_4	REG_B(DDRA,4)
#define DDRA_5	REG_B(DDRA,5)
#define DDRA_6	REG_B(DDRA,6)
#define DDRA_7	REG_B(DDRA,7)      

/* PORT B */
#define PORTB_0	REG_B(PORTB,0)
#define PORTB_1	REG_B(PORTB,1)
#define PORTB_2	REG_B(PORTB,2)
#define PORTB_3	REG_B(PORTB,3)
#define PORTB_4	REG_B(PORTB,4)
#define PORTB_5	REG_B(PORTB,5)
#define PORTB_6	REG_B(PORTB,6)
#define PORTB_7	REG_B(PORTB,7)
            
#define PINB_0	REG_B(PINB,0)
#define PINB_1	REG_B(PINB,1)
#define PINB_2	REG_B(PINB,2)
#define PINB_3	REG_B(PINB,3)
#define PINB_4	REG_B(PINB,4)
#define PINB_5	REG_B(PINB,5)
#define PINB_6	REG_B(PINB,6)
#define PINB_7	REG_B(PINB,7)
           
#define DDRB_0	REG_B(DDRB,0)
#define DDRB_1	REG_B(DDRB,1)
#define DDRB_2	REG_B(DDRB,2)
#define DDRB_3	REG_B(DDRB,3)
#define DDRB_4	REG_B(DDRB,4)
#define DDRB_5	REG_B(DDRB,5)
#define DDRB_6	REG_B(DDRB,6)
#define DDRB_7	REG_B(DDRB,7)
           
/* PORT C */
#define PORTC_0	REG_B(PORTC,0)
#define PORTC_1	REG_B(PORTC,1)
#define PORTC_2	REG_B(PORTC,2)
#define PORTC_3	REG_B(PORTC,3)
#define PORTC_4	REG_B(PORTC,4)
#define PORTC_5	REG_B(PORTC,5)
#define PORTC_6	REG_B(PORTC,6)
#define PORTC_7	REG_B(PORTC,7)
            
#define PINC_0	REG_B(PINC,0)
#define PINC_1	REG_B(PINC,1)
#define PINC_2	REG_B(PINC,2)
#define PINC_3	REG_B(PINC,3)
#define PINC_4	REG_B(PINC,4)
#define PINC_5	REG_B(PINC,5)
#define PINC_6	REG_B(PINC,6)
#define PINC_7	REG_B(PINC,7)
           
#define DDRC_0	REG_B(DDRC,0)
#define DDRC_1	REG_B(DDRC,1)
#define DDRC_2	REG_B(DDRC,2)
#define DDRC_3	REG_B(DDRC,3)
#define DDRC_4	REG_B(DDRC,4)
#define DDRC_5	REG_B(DDRC,5)
#define DDRC_6	REG_B(DDRC,6)
#define DDRC_7	REG_B(DDRC,7)
          
/* PORT D */
#define PORTD_0	REG_B(PORTD,0)
#define PORTD_1	REG_B(PORTD,1)
#define PORTD_2	REG_B(PORTD,2)
#define PORTD_3	REG_B(PORTD,3)
#define PORTD_4	REG_B(PORTD,4)
#define PORTD_5	REG_B(PORTD,5)
#define PORTD_6	REG_B(PORTD,6)
#define PORTD_7	REG_B(PORTD,7)
            
#define PIND_0	REG_B(PIND,0)
#define PIND_1	REG_B(PIND,1)
#define PIND_2	REG_B(PIND,2)
#define PIND_3	REG_B(PIND,3)
#define PIND_4	REG_B(PIND,4)
#define PIND_5	REG_B(PIND,5)
#define PIND_6	REG_B(PIND,6)
#define PIND_7	REG_B(PIND,7)
           
#define DDRD_0	REG_B(DDRD,0)
#define DDRD_1	REG_B(DDRD,1)
#define DDRD_2	REG_B(DDRD,2)
#define DDRD_3	REG_B(DDRD,3)
#define DDRD_4	REG_B(DDRD,4)
#define DDRD_5	REG_B(DDRD,5)
#define DDRD_6	REG_B(DDRD,6)
#define DDRD_7	REG_B(DDRD,7)

/* PORT E */
#define PORTE_0	REG_B(PORTE,0)
#define PORTE_1	REG_B(PORTE,1)
#define PORTE_2	REG_B(PORTE,2)
#define PORTE_3	REG_B(PORTE,3)
#define PORTE_4	REG_B(PORTE,4)
#define PORTE_5	REG_B(PORTE,5)
#define PORTE_6	REG_B(PORTE,6)
#define PORTE_7	REG_B(PORTE,7)
            
#define PINE_0	REG_B(PINE,0)
#define PINE_1	REG_B(PINE,1)
#define PINE_2	REG_B(PINE,2)
#define PINE_3	REG_B(PINE,3)
#define PINE_4	REG_B(PINE,4)
#define PINE_5	REG_B(PINE,5)
#define PINE_6	REG_B(PINE,6)
#define PINE_7	REG_B(PINE,7)
           
#define DDRE_0	REG_B(DDRE,0)
#define DDRE_1	REG_B(DDRE,1)
#define DDRE_2	REG_B(DDRE,2)
#define DDRE_3	REG_B(DDRE,3)
#define DDRE_4	REG_B(DDRE,4)
#define DDRE_5	REG_B(DDRE,5)
#define DDRE_6	REG_B(DDRE,6)
#define DDRE_7	REG_B(DDRE,7)

/* PORT F */
#define PORTF_0	REG_B(PORTF,0)
#define PORTF_1	REG_B(PORTF,1)
#define PORTF_2	REG_B(PORTF,2)
#define PORTF_3	REG_B(PORTF,3)
#define PORTF_4	REG_B(PORTF,4)
#define PORTF_5	REG_B(PORTF,5)
#define PORTF_6	REG_B(PORTF,6)
#define PORTF_7	REG_B(PORTF,7)
            
#define PINF_0	REG_B(PINF,0)
#define PINF_1	REG_B(PINF,1)
#define PINF_2	REG_B(PINF,2)
#define PINF_3	REG_B(PINF,3)
#define PINF_4	REG_B(PINF,4)
#define PINF_5	REG_B(PINF,5)
#define PINF_6	REG_B(PINF,6)
#define PINF_7	REG_B(PINF,7)
           
#define DDRF_0	REG_B(DDRF,0)
#define DDRF_1	REG_B(DDRF,1)
#define DDRF_2	REG_B(DDRF,2)
#define DDRF_3	REG_B(DDRF,3)
#define DDRF_4	REG_B(DDRF,4)
#define DDRF_5	REG_B(DDRF,5)
#define DDRF_6	REG_B(DDRF,6)
#define DDRF_7	REG_B(DDRF,7)

/* PORT H */
#define PORTH_0	REG_B(PORTH,0)
#define PORTH_1	REG_B(PORTE,1)
#define PORTH_2	REG_B(PORTH,2)
#define PORTH_3	REG_B(PORTH,3)
#define PORTH_4	REG_B(PORTH,4)
#define PORTH_5	REG_B(PORTH,5)
#define PORTH_6	REG_B(PORTH,6)
#define PORTH_7	REG_B(PORTH,7)
            
#define PINH_0	REG_B(PINH,0)
#define PINH_1	REG_B(PINH,1)
#define PINH_2	REG_B(PINH,2)
#define PINH_3	REG_B(PINH,3)
#define PINH_4	REG_B(PINH,4)
#define PINH_5	REG_B(PINH,5)
#define PINH_6	REG_B(PINH,6)
#define PINH_7	REG_B(PINH,7)
           
#define DDRH_0	REG_B(DDRH,0)
#define DDRH_1	REG_B(DDRH,1)
#define DDRH_2	REG_B(DDRH,2)
#define DDRH_3	REG_B(DDRH,3)
#define DDRH_4	REG_B(DDRH,4)
#define DDRH_5	REG_B(DDRH,5)
#define DDRH_6	REG_B(DDRH,6)
#define DDRH_7	REG_B(DDRH,7)


#endif
           