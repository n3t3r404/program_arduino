/*************************************************************

 Create with PROGRAMINO-IDE http://www.programino.com
 
 Project:       Programino Plotter Demo
 Librarys:      
 Author:        UlliS
 Description:   
 
**************************************************************/

byte const Button = 8;

void setup() {

    // Setup code
    Serial.begin(115200);            // set baudrate
    pinMode(Button,INPUT);           // pin 8 is input
    digitalWrite(Button,HIGH);       // activate internal pullup restister

}

void loop() {

    // 1 Channel Demo
    // Sinus Demo (values between -10 and +10)
    // ---------------------------------------
    for(int j=0;j<360;j++)
    {
        Serial.println(sin(j*(PI/180))*100);
        delay(10);
    }   


    //// Demo negative and positiv values
    //// Digital Demo (value -10 or +10)
    //// -------------------------------
    //delay(100); 
    //Serial.println("10");  
    //delay(100);
    //Serial.println(0);
    //delay(100)
    //Serial.println("-10");
    //delay(100);


    //// Demo show analog values from ADC
    //// Analog Demo (read the analog input A0, values between 0 and 1023)
    //// -----------------------------------------------------------------
    //Serial.println(analogRead(A0));
    //delay(10);


    //// Demo show button state
    //// Digital Demo (read the button on pin 8 switch to ground, value 0 or 1)
    //// ----------------------------------------------------------------------
    //Serial.println(digitalRead(Button));
    //delay(10);

}
